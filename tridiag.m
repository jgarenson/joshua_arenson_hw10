function T = tridiag(A)
%reduce symmetric matrix A to tridiagonal form.

if size(A,1) ~= size(A,2)
    error('A is not a square matrix')
end

if any((A == A') == 0)
    error('A is not symmetric.')
end

m=length(A);

for k = 1:m-2
    x = A(k+1:m,k);
    v = x;
    v(1) = x(1) + sign(x(1))*norm(x);
    v = v/norm(v);
    A(k+1:m,k:m) = A(k+1:m,k:m) -2*v*(v'*A(k+1:m,k:m));
    A(1:m,k+1:m) = A(1:m,k+1:m) -2*(A(1:m,k+1:m)*v)*v';
end
T = A;
end
