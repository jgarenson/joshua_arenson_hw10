function Tnew = qralg(T)
%Applies 28.1 to a tridiagonal matrix T.

if size(T,1) ~= size(T,2)
    error('A is not a square matrix')
end


%use matlab inbuilt qr function (faster)
[Q,R] = qr(T);

%A1 is reverse QR
T = R*Q;

Tnew = T;
