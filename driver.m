%"drives" the tridiag and qralg functions to calculate eigenvalues for A.

% A = hilb(4);
A = diag(15:-1:1) + ones(15,15);


%tridiagonalize
T = tridiag(A);

%number of iterates desired
num_cycles = 50;
%preallocate error space
err = zeros(num_cycles);

%run qralg for the number of iterates storing error number
for k = 1:num_cycles
    T = qralg(T);
    err(k) = abs(T(end,end-1));
end

%prefigure
figure
title('Error versus number of iterates')
xlabel('Number of Iterates')
ylabel('Log of Error')

%semilog plot
semilogy(1:num_cycles,err,'-k.')