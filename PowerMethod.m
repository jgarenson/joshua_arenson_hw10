function [v1,lambda] = PowerMethod(A,v0)
%Completes one Iteration of the power method for nxn matrix A with initial
%vector v0. Outputs v1 and it's rayleight quotient

[m,n] = size(A);
if m ~= n
    error('A is not a square matrix')
end

if size(v0,2) ~= 1
    error('v is not a column vector.')
end

%find unormalized v1
w = A*v0;
%normalize
v1 = w/norm(w);
%calculate rayleight quotient
lambda = v1'*A*v1;
end
