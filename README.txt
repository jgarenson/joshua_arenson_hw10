driver.m is the main running program for problem 8. It calls tridiag.m to find the tridiagonal of a matrix then qralg to calculate eigenvalues and error.

PowerMethod.m Uses the power method to calculate eigenvectors of A

Pure_QR.m Does a single iterate of QR method to calculate eigenvalues.

qralg.m Does a single iterate of QR method to calculate eigenvalues for a tridiagonal matrix.

tridiag.m Takes a symmetric matrix and reduces it to a tridiagonal matrix.
