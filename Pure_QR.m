function A1 = Pure_QR(A)
%Applies one iterate of pure QR to calculate A1 for nxn matrix A

if size(A,1) ~= size(A,2)
    error('A is not a square matrix.')
end

A0 = A;

%use matlab inbuilt qr function (faster)
[Q,R] = qr(A);

%A1 is reverse QR
A1 = R*Q;

